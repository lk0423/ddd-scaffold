package com.lk.basics.web.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author lk
 */
@Data
public class NoteVO {

    @ApiModelProperty("ID")
    private Long id;

    @ApiModelProperty("章节id")
    private Long sectionId;

    @ApiModelProperty("笔记名称")
    private String name;

    @ApiModelProperty("附件笔记")
    private String noteUrl;

    @ApiModelProperty("封面")
    private String coverUrl;

    @ApiModelProperty("笔记详情")
    private String detail;
}
