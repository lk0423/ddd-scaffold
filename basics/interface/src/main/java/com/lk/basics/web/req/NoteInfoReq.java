package com.lk.basics.web.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author lk
 */
@Data
public class NoteInfoReq {

    @ApiModelProperty("ID")
    private Long id;

    @ApiModelProperty("笔记名称")
    private String name;

    @ApiModelProperty("附件笔记")
    private String noteUrl;

    @ApiModelProperty("封面")
    private String coverUrl;

    @ApiModelProperty("笔记详情")
    private String detail;

}
