package com.lk.basics.web.facade.assembler;

import com.lk.basics.domain.module.NoteInfo;
import com.lk.basics.web.req.NoteInfoReq;
import com.lk.basics.web.vo.NoteVO;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.Optional;

/**
 * @author lk
 * @date 2021/7/29 10:50 上午
 */
@Component
public class NoteAssembler {

    public static NoteVO domainToVO(NoteInfo noteInfo) {
        NoteVO noteVO = new NoteVO();
        noteVO.setId(noteInfo.getId());
        noteVO.setName(noteInfo.getName());
        noteVO.setSectionId(noteInfo.getSectionId());
        noteVO.setNoteUrl(noteInfo.getNoteUrl());
        noteVO.setCoverUrl(noteInfo.getCoverUrl());
        noteVO.setDetail(noteInfo.getDetail());
        return noteVO;
    }

    public static NoteInfo voToDomain(NoteInfoReq noteInfoReq) {
        if (Objects.isNull(noteInfoReq)) {
            return null;
        }
        return Optional.of(
                NoteInfo.builder()
                        .id(noteInfoReq.getId())
                        .name(noteInfoReq.getName())
                        .noteUrl(noteInfoReq.getNoteUrl())
                        .coverUrl(noteInfoReq.getCoverUrl())
                        .detail(noteInfoReq.getDetail())
                        .build()
        ).orElse(null);
    }
}
