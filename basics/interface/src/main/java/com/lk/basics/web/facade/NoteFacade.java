package com.lk.basics.web.facade;

import com.lk.basics.domain.module.NoteInfo;
import com.lk.basics.domain.module.NoteRepository;
import com.lk.basics.web.facade.assembler.NoteAssembler;
import com.lk.basics.web.vo.NoteVO;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Objects;

/**
 * @author lk
 * @date 2021/7/28 2:07 下午
 */
@Component
public class NoteFacade {

    @Resource
    private NoteRepository noteRepository;

    public NoteVO getById(Long noteId){
        NoteInfo noteInfo = noteRepository.getById(noteId);
        if (Objects.isNull(noteInfo)){
            return null;
        }
        return NoteAssembler.domainToVO(noteInfo);
    }

}
