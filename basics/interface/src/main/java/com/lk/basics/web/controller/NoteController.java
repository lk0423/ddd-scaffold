package com.lk.basics.web.controller;

import com.lk.basics.application.service.NoteService;
import com.lk.basics.web.facade.NoteFacade;
import com.lk.basics.web.facade.assembler.NoteAssembler;
import com.lk.basics.web.req.NoteInfoReq;
import com.lk.basics.web.vo.NoteVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author lk
 * @date 2021/7/27 10:34 上午
 */
@Api(tags = "笔记")
@Slf4j
@RestController
@RequestMapping(value = "/web/learning/note")
public class NoteController {

    @Resource
    private NoteFacade noteFacade;
    @Resource
    private NoteService noteService;


    @ApiOperation("新建")
    @PostMapping("/save")
    public Long saveNote(@RequestBody NoteInfoReq noteInfo) {
        return noteService.saveNote(NoteAssembler.voToDomain(noteInfo));
    }

    @ApiOperation("查询笔记")
    @GetMapping("/getById/{noteId}")
    public NoteVO getById(@PathVariable("noteId") Long noteId) {
        return noteFacade.getById(noteId);
    }
}
