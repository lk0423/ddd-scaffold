package com.lk.basics.application.service;

import com.lk.basics.domain.module.NoteInfo;
import com.lk.basics.domain.module.NoteRepository;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author lk
 */
@Service
public class NoteService {

    @Resource
    private NoteRepository noteRepository;

    public Long saveNote(NoteInfo noteInfo) {
        return noteRepository.save(noteInfo);
    }
}
