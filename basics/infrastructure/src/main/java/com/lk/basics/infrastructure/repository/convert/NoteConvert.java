package com.lk.basics.infrastructure.repository.convert;


import com.lk.basics.domain.module.NoteInfo;
import com.lk.basics.infrastructure.persistence.mybatis.dataobject.NoteDO;

import java.util.Objects;
import java.util.Optional;

/**
 * @author lk
 */
public class NoteConvert {

    public static NoteInfo doToNoteInfo(NoteDO noteDO) {
        if (Objects.isNull(noteDO)) {
            return null;
        }
        return Optional.of(
                NoteInfo.builder()
                        .id(noteDO.getId())
                        .sectionId(noteDO.getSectionId())
                        .name(noteDO.getName())
                        .noteUrl(noteDO.getNoteUrl())
                        .coverUrl(noteDO.getCoverUrl())
                        .detail(noteDO.getDetail())
                        .build()
        ).orElse(null);
    }


    public static NoteDO toNoteDO(NoteInfo noteInfo) {
        NoteDO noteDO = new NoteDO();
        noteDO.setId(noteInfo.getId())
                .setSectionId(noteInfo.getSectionId())
                .setName(noteInfo.getName())
                .setNoteUrl(noteInfo.getNoteUrl())
                .setCoverUrl(noteInfo.getCoverUrl())
                .setDetail(noteInfo.getDetail());
        return noteDO;
    }
}
