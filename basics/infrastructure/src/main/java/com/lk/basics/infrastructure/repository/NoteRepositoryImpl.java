package com.lk.basics.infrastructure.repository;


import com.lk.basics.domain.module.NoteInfo;
import com.lk.basics.domain.module.NoteRepository;
import com.lk.basics.infrastructure.persistence.mybatis.dataobject.NoteDO;
import com.lk.basics.infrastructure.persistence.mybatis.mapper.NoteMapper;
import com.lk.basics.infrastructure.repository.convert.NoteConvert;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.Date;
import java.util.Objects;

/**
 * @author lk
 */
@Repository("NoteRepository")
public class NoteRepositoryImpl implements NoteRepository {

    @Resource
    private NoteMapper noteMapper;

    @Override
    public Long save(NoteInfo noteInfo) {
        if (Objects.isNull(noteInfo)){
            return null;
        }
        NoteDO noteDO = NoteConvert.toNoteDO(noteInfo);
        noteDO.setCreatedTime(new Date());
        noteDO.setCreatedBy(noteInfo.getOperateUser());
        noteMapper.insert(noteDO);
        return noteDO.getId();
    }

    @Override
    public NoteInfo getById(Long id) {
        if (Objects.isNull(id)) {
            return null;
        }
        return NoteConvert.doToNoteInfo(noteMapper.selectById(id));
    }

}
