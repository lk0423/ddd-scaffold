package com.lk.basics.infrastructure.persistence.mybatis.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lk.basics.infrastructure.persistence.mybatis.dataobject.NoteDO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Select;

import java.util.Collection;
import java.util.List;

/**
 * <p>
 * 笔记 Mapper 接口
 * </p>
 *
 * @author lk
 */
@Mapper
public interface NoteMapper extends BaseMapper<NoteDO> {
    String DEFAULT_COLUMN_LIST = "id, section_id, note_url, cover_url, name, detail, created_by, update_by, created_time, update_time";

    @Select(" <script>" +
            " SELECT " + DEFAULT_COLUMN_LIST + " FROM dealer_learning_note" +
            " WHERE section_id in" +
            " <foreach collection='sectionIds' separator=',' open='(' close=')' item='item'>" +
            " #{item}" +
            " </foreach>" +
            "</script>")
    @ResultType(value = NoteDO.class)
    List<NoteDO> getBySectionIds(@Param("sectionIds") Collection<Long> sectionIds);
}
