package com.lk.basics.infrastructure.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @author lk
 */
@Component
@Slf4j
public class MyBatisMetaObjectHandler implements MetaObjectHandler {


    @Override
    public void insertFill(MetaObject metaObject) {
        log.info("insert fileTable ====> {}",metaObject);
        if (getFieldValByName("createTime", metaObject) == null) {
            this.setFieldValByName("createTime", new Date(), metaObject);
        }
        if (getFieldValByName("updateTime", metaObject) == null) {
            this.setFieldValByName("updateTime", new Date(), metaObject);
        }
        if (getFieldValByName("isDeleted", metaObject) == null) {
            this.setFieldValByName("isDeleted", Boolean.FALSE, metaObject);
        }
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        if (getFieldValByName("updateTime", metaObject) == null) {
            this.setFieldValByName("updateTime", new Date(), metaObject);
        }
    }


}