package com.lk.basics.domain.module;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

/**
 * @author lk
 */
@Data
@Builder
public class NoteInfo {

    @ApiModelProperty("ID")
    private Long id;

    @ApiModelProperty("章节id")
    private Long sectionId;

    @ApiModelProperty("笔记名称")
    private String name;

    @ApiModelProperty("附件笔记")
    private String noteUrl;

    @ApiModelProperty("封面")
    private String coverUrl;

    @ApiModelProperty("笔记详情")
    private String detail;

    @ApiModelProperty("擦作者")
    private String operateUser;
}
