package com.lk.basics.domain.module;

import org.springframework.stereotype.Repository;

/**
 * @author lk
 */
@Repository
public interface NoteRepository {

    /**
     * 保存
     *
     * @param noteInfo
     * @return
     */
    Long save(NoteInfo noteInfo);

    /**
     * 查询
     *
     * @param id
     * @return
     */
    NoteInfo getById(Long id);

}
